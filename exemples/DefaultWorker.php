<?php
/**
 * Exemple d'un worker
 *
 * Donne un exemple d'utilisation de worker. Celui-ci calculera la quantitée
 * de nombre premiers en fonction d'un message Beanstalk qui indique un nombre.
 * Le worker calculera les nombres premiers inferieur au nombre indiqué.
 * ex: si 10 est envoyé en message, [2,3,5,7] donc 4 nombre premiers trouvé.
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace App\Shell\Worker;

use Beanstalk\Command\AbstractWorker;
use Beanstalk\Utility\Beanstalk;

/**
 * Classe d'exemple d'un worker.
 *
 * Possède les fonction <b>work()</b> et <b>keepWorking()</b> obligatoire.
 * <b>work()</b> execute le travail à accomplir. Les exception qu'il envoi sont capturées
 * par AbstractWorker qui emet un message d'erreur via Beanstalk.
 *
 * <b>keepWorking()</b> permet de continuer ou pas un nouveau cicle de travail.
 * Par défaut, il dépendra de la conf sur l'attribut 'keep_working' mais on peut
 * décider d'un fonctionnement different.
 *
 * <b>nb_premiers()</b> permet bien evidement de calculer les nombre premiers,
 * et est une fonction qui ne dépend que de ce worker.
 */
class DefaultWorker
{
    /**
     * {@inheritdoc}
     */
    public function work($data)
    {
        if (is_numeric($data)) {
            $nombres = $this->nbPremiers($data);

            $count = count($nombres);
            $result = [
                'worker' => __CLASS__,
                'message' => sprintf("Nombre premiers calculé avec succès. "
                    . "Il y a %d nombres premiers", $count),
                'pid' => getmypid(),
                'raw' => $nombres
            ];

            var_dump($result);
        } else {
            throw new \Exception("Le message doit être numérique !");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function keepWorking($params)
    {
        return parent::keepWorking($params);
    }

    /**
     * Donne une liste de nombre premiers
     *
     * @param integer $limit nombre premiers de 1 à $limit
     * @return array
     */
    protected function nbPremiers($limit = 100)
    {
        $premiers = [];

        for ($i = 2; $i < $limit; $i++) {
            $isPremier = true;
            for ($j = $i-1; $j > 1; $j--) {
                if (round($i / $j) == $i / $j) {
                    $isPremier = false;
                }
            }

            if ($isPremier) {
                $premiers[] = $i;
            }
        }

        return $premiers;
    }
}
