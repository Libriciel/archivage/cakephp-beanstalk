FROM php:8.1.16-cli as baseimage

ARG timezone=Europe/Paris

# upgrade
RUN ln -snf /usr/share/zoneinfo/$timezone /etc/localtime && echo $timezone > /etc/timezone
RUN apt update \
    && apt install -y libzmq3-dev git vim sudo bash-completion zip sqlite3 locales postgresql libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install sockets intl pdo_pgsql \
    && sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales

# Install php-zmq
RUN cd /opt; \
    git clone https://github.com/zeromq/php-zmq.git; \
    cd php-zmq; \
    phpize && ./configure; \
    make; \
    make install \
    && echo "extension=zmq" >> /usr/local/etc/php/conf.d/zmq.ini

# composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html

CMD ["bin/cake", "beanstalk_server"]

#-------------------------------------------------------------------------------

FROM baseimage AS dev

COPY --chown=www-data:www-data . /var/www/html/
RUN echo $(date) > /var/www/html/.image-dev

WORKDIR /var/www/html

CMD ["bin/cake", "beanstalk_server"]
