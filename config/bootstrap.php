<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

if (!defined('ROOT')) {

    define('ROOT', dirname(__DIR__));
    define('APP_DIR', 'src');
    define('APP', ROOT . DS . APP_DIR . DS);
    define('CONFIG', ROOT . DS . 'config' . DS);
    define('WWW_ROOT', ROOT . DS . 'webroot' . DS);
    define('TESTS', ROOT . DS . 'tests' . DS);
    define('TMP', ROOT . DS . 'tmp' . DS);
    define('LOGS', ROOT . DS . 'logs' . DS);
    define('CACHE', TMP . 'cache' . DS);
    define('CAKE_CORE_INCLUDE_PATH', ROOT . DS . 'vendor' . DS . 'cakephp' . DS . 'cakephp');
    define('CORE_PATH', CAKE_CORE_INCLUDE_PATH . DS);
    define('CAKE', CORE_PATH . 'src' . DS);

    include CORE_PATH . 'config/bootstrap.php';
    if (is_file(CONFIG . 'app.php')) {
        Configure::load('app', 'default', false);
        ConnectionManager::setConfig(Configure::consume('Datasources'));
    }

    $_SERVER['PHP_SELF'] = '/';

    define('DATABASE_TEST_SQLITE', sys_get_temp_dir().DS.'beanstalk.sqlite');
    if (is_file(DATABASE_TEST_SQLITE)) {
        unlink(DATABASE_TEST_SQLITE);
        touch(DATABASE_TEST_SQLITE);
    }
    if (extension_loaded('sqlite3')) {
        try {
            Cake\Datasource\ConnectionManager::get('default');
        } catch (Exception $e) {
            Cake\Datasource\ConnectionManager::setConfig(
                'default',
                [
                    'className' => Cake\Database\Connection::class,
                    'driver' => Cake\Database\Driver\Sqlite::class,
                    'database' => DATABASE_TEST_SQLITE,
                ]
            );
        }
        try {
            Cake\Datasource\ConnectionManager::get('test');
        } catch (Exception $e) {
            Cake\Datasource\ConnectionManager::setConfig(
                'test',
                [
                    'className' => Cake\Database\Connection::class,
                    'driver' => Cake\Database\Driver\Sqlite::class,
                    'database' => DATABASE_TEST_SQLITE,
                ]
            );
        }
    }

    if (!Configure::read('App')) {
        Configure::write(
            [
                'debug' => true,
                'App' => [
                    'namespace' => 'Beanstalk',
                    'encoding' => env('APP_ENCODING', 'UTF-8'),
                    'defaultLocale' => env('APP_DEFAULT_LOCALE', 'fr_FR'),
                    'base' => false,
                    'dir' => 'src',
                    'webroot' => 'webroot',
                    'wwwRoot' => WWW_ROOT,
                    'fullBaseUrl' => false,
                    'imageBaseUrl' => 'img/',
                    'cssBaseUrl' => 'css/',
                    'jsBaseUrl' => 'js/',
                    'paths' => [
                        'plugins' => [ROOT . DS . 'plugins' . DS],
                        'templates' => [APP . 'Template' . DS],
                        'locales' => [APP . 'Locale' . DS],
                    ],
                ],
            ]
        );
        Cache::setConfig(
            [
                'default' => [
                    'className' => 'File',
                    'path' => CACHE,
                    'url' => env('CACHE_DEFAULT_URL', null),
                ],
                '_cake_core_' => [
                    'className' => 'File',
                    'prefix' => 'myapp_cake_core_',
                    'path' => CACHE . 'persistent/',
                    'serialize' => true,
                    'duration' => '+1 years',
                    'url' => env('CACHE_CAKECORE_URL', null),
                ],
                '_cake_model_' => [
                    'className' => 'File',
                    'prefix' => 'myapp_cake_model_',
                    'path' => CACHE . 'models/',
                    'serialize' => true,
                    'duration' => '+1 years',
                    'url' => env('CACHE_CAKEMODEL_URL', null),
                ],
            ]
        );
    }
}
