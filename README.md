# Beanstalk
[![License](https://img.shields.io/badge/licence-CeCILL%20v2-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.txt)
[![build status](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk/pipelines/latest)
[![coverage report](https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk/badges/master/coverage.svg)](https://asalae2.dev.libriciel.fr/coverage/cakephp-beanstalk/index.html)

Plugin Cakephp 4.

Interface entre le service __beanstalkd__, votre application et vos workers.  


## Prérequis

Un serveur Beanstalkd est indispensable.
```bash
sudo apt-get install beanstalkd
```

Si vous voulez conserver les jobs après le redémarrage du service, il vous faut
modifier la conf.
```bash
sudo nano /etc/default/beanstalkd
```

Décommettez la dernière ligne :
`BEANSTALKD_EXTRA="-b /var/lib/beanstalkd"`

Autre méthode : lancez __beanstalkd__ avec l'option __-d__


## Installation

```bash
composer config repositories.libriciel/cakephp-beanstalk git https://gitlab.libriciel.fr/CakePHP/cakephp-beanstalk.git
composer require libriciel/cakephp-beanstalk ^2.0
```

Ajoutez le plugin dans votre Application.php

```php
$this->addPlugin('Beanstalk');

```

### Base de données

Vous pouvez au choix, utiliser le SQL (postgres) fourni dans **config/schema/beanstalk.sql**, ou bien utiliser le plugin migration :
```bash
bin/cake migrations migrate --plugin Beanstalk
```

## Utilisation

Maintenant que vous disposez de votre serveur Beanstalkd, il va vous falloir
un ou plusieurs workers selon le besoin.

Exemple sur vendor/libriciel/cakephp-beanstalk/exemple/DefaultWorker.php
Copiez ce fichier vers src/Shell/Worker
Ajoutez à votre configuration :
```php
Configure::write([
    'Beanstalk' => [
        /**
         * Connection au serveur Beanstalkd
         */
        'host' => '127.0.0.1',
        'port' => 11300,
        'timeout' => null,
        'persistant' => false,

        /**
         * Chemin des classes de workers
         */
        'workerPaths' => [
            APP . 'Shell' . DS . 'Worker'
        ],

        /**
         * Configurations liées aux tests des workers
         *
         * - timeout: durée maximum en secondes entre l'émission du test et la
         *            consultation du résultat
         */
        'tests' => [
            'timeout' => 10,
        ],
        
        /**
         * Configuration optionnelle
         */
         'table_jobs' => 'Beanstalk.BeanstalkJobs',
         'table_workers' => 'Beanstalk.BeanstalkWorkers',
         'classname', \Beanstalk\Utility\Beanstalk::class,
         'PheanstalkClassname' => \Pheanstalk\Pheanstalk::class
    ]
]);
```

### Lancer un worker

__Options:__

- __--dir__                Dossier de la classe du worker
- __--one-job__            N'effectue qu'un seul job - utile pour un worker sous docker
- __--suffix__             Permet de spécifier le dossier worker (default: Worker)
- __--table-workers__      Permet de spécifier la table utilisée pour stocker les informations sur le worker lancé (default: Beanstalk.BeanstalkWorkers)
- __--tube__               Nom du tube (par défaut: _nom du worker_)
- __--unique__             Lance le worker seulement s'il est seul sur son tube


__Arguments:__

- __worker__  Nom du worker (optional)

Lancez votre shell :
```bash
bin/cake worker default_worker
```

On peut lancer plusieurs workers en même temps. Attention car un worker peut
occuper un thread à 100% jusqu’à la fin de la file de _job_. Si le nombre de
workers dépasse celui du nombre de cœur la charge sera équitablement répartie.
Si les workers sont sur le même serveur que la page web, l’expérience
utilisateur peut s'en retrouver affectée.

## L'utilitaire Beanstalk

Permet de communiquer avec le service __Beanstalkd__ tout en manipulant la base de données de l'application afin de conserver certaines informations. 

### La methode __\_\_construct()__

Prends en paramètre le tube sur lequel va travailler le worker. Il est bon
d'avoir un tube par worker.
Les autres paramètres sont pour la connexion au serveur __beanstalkd__.

### emit()

Ajoute un Job dans le tube, avec le data passé en paramètres.

### getNext()

Utile pour boucler dessus, il se charge de réserver le prochain job et l'affecter
à `Worker->job`
Si aucun job n'est à faire, il attend tranquillement qu'un nouveau job arrive
sans prendre de la charge serveur.

### getData()

Attention, peut renvoyer faux si la signature des données n'est pas validée.
Cette particularité a pour but d'éviter les conflits si une autre application
utilise le même tube.

Sinon, la donnée renvoyée peut être de tous types grâce à une conversion json.

### done()

Supprime le job car celui-ci est terminé


### exemple:

```php
$Beanstalk = new \Beanstalk\Utility\Beanstalk('mon-tube');

$Beanstalk->emit('foo'); // Envoi un message dans le tube

while ($Beanstalk->getNext()) {
    $data = $Beanstalk->getData();
    $Beanstalk->done();
    if ($data === 'foo') {
        break;
    }
}
```

## Faire un worker

Créez une classe ___\<NomDu\>Worker___ (suffixe _Worker_ par défaut) qui implémente __WorkerInterface__.
Vous pouvez étendre la classe __AbstractWorker__ qui possède déjà la mécanique de base d'un worker (il vous faudra juste définir la méthode principale: work()).

### exemple:
```php
<?php
namespace App\Shell\Worker;
use Beanstalk\Command\AbstractWorker;
use Beanstalk\Command\WorkerInterface;

class ExempleWorker extends AbstractWorker implements WorkerInterface
{
    public function work($data)
    {
        echo $data;
    }
}
```
