<?php
/**
 * Beanstalk\Command\JobListCommand
 */

namespace Beanstalk\Command;

use Beanstalk\Console\ConsoleOutput;
use Beanstalk\Model\Table\BeanstalkJobsTable;
use Beanstalk\Model\Table\BeanstalkWorkersTable;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\FrozenTime;
use DateInterval;
use DateTime;
use Exception;
use React\EventLoop\Loop;
use React\Socket\ConnectionInterface;
use React\Socket\TcpServer;
use RuntimeException;
use ZMQ;
use ZMQContext;
use ZMQSocket;
use ZMQSocketException;

/**
 * Permet de liste les jobs
 * ex: bin/cake job list test --page 1
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class BeanstalkServerCommand extends Command
{
    /**
     * Durée de rafraichissement en secondes
     */
    const REFRESH_INTERVAL = 1.0;
    /**
     * liste les connections des workers
     * @var array [$tube => [$workerId => $connection], ...]
     */
    private array $workers = [];
    /**
     * @var ConsoleIo
     */
    private ConsoleIo $io;
    /**
     * @var array
     */
    private array $config;
    /**
     * @var array
     */
    private array $waitingWorkers = [];
    /**
     * @var int|null
     */
    private ?int $configMtime;
    /**
     * @var ZMQSocket|null
     */
    private ?ZMQSocket $ratchetSocket;
    /**
     * @var array
     */
    private array $modelParams = [];
    /**
     * @var ConsoleOutput
     */
    private ConsoleOutput $out;
    /**
     * @var ConsoleOutput
     */
    private ConsoleOutput $err;

    /**
     * Run the command contained in $argv.
     *
     * Use the application to do the following:
     *
     * - Bootstrap the application
     * - Create the CommandCollection using the console() hook on the application.
     * - Trigger the `Console.buildCommands` event of auto-wiring plugins.
     * - Run the requested command.
     *
     * @param array          $argv The arguments from the CLI environment.
     * @param ConsoleIo|null $io   The ConsoleIo instance. Used primarily for testing.
     * @return int The exit code of the command.
     * @throws RuntimeException
     */
    public function run(array $argv, ?ConsoleIo $io = null): int
    {
        if (in_array('--log', $argv)) {
            $this->out = new ConsoleOutput('php://stdout');
            $this->err = new ConsoleOutput('php://stderr');
            $io = new ConsoleIo($this->out, $this->err);
        }
        return parent::run($argv, $io);
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addOption(
            'datasource',
            [
                'help' => __("Datasource utilisé"),
                'default' => 'default',
            ]
        );
        $parser->addOption(
            'log',
            [
                'help' => __("Fichier de log additionnel"),
            ]
        );
        return $parser;
    }

    /**
     * Start the Command and interactive console.
     *
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @return void The exit code or null for success
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->io = $io;
        if ($logfile = $args->getOption('log')) {
            $this->out->setExternalLog($logfile);
            $this->err->setExternalLog($logfile);
        }
        $datasource = $args->getOption('datasource');
        if ($datasource !== 'default') {
            $this->modelParams = ['connectionName' => $datasource];
        }
        $this->setRatchet();
        $jsonPath = $this->getConfigFilename();
        $this->configMtime = $jsonPath ? $this->filemtime($jsonPath) : null;
        $this->config = Configure::read('Beanstalk') + [
            'server_host' => '127.0.0.1',
            'server_port' => 11300,
        ];
        $loop = Loop::get();
        $uri = sprintf('tcp://%s:%d', $this->config['server_host'], $this->config['server_port']);
        $server = new TcpServer($uri, $loop);
        $server
            ->on(
                'connection',
                function (ConnectionInterface $connection) {
                    $this->io->verbose("new connection: {$connection->getRemoteAddress()}");
                    $connection->on(
                        'data',
                        function ($data) use ($connection) {
                            $this->io->verbose("data: $data");
                            $data = trim($data);
                            foreach (explode("\r\n", $data) as $message) {
                                $this->handleIncomingMessage($message, $connection);
                            }
                        }
                    );
                    $connection->on(
                        'close',
                        function () use ($connection) {
                            $this->io->verbose("closing connection: {$connection->getRemoteAddress()}");
                            $this->removeRef($connection);
                        }
                    );
                }
            );
        $io->out('listening on ' . $this->config['server_port']);
        $loop->addPeriodicTimer(
            self::REFRESH_INTERVAL,
            [$this, 'checkReadyJobs']
        );
        $loop->run();
    }

    /**
     * Gestion des messages entrants
     * @param string              $data
     * @param ConnectionInterface $connection
     * @return void
     * @throws Exception
     */
    private function handleIncomingMessage(string $data, ConnectionInterface $connection)
    {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs', $this->modelParams);
        $datas = explode(':', trim($data), 3);
        if (!$datas) {
            $this->io->warning("empty message");
            $connection->end();
            return;
        } elseif ($datas[0] === 'job' && count($datas) === 3) {
            $tube = $datas[1];
            $job = $BeanstalkJobs->find()->where(['id' => $datas[2]])->first();
            if (!$job) {
                return;
            }
            if (isset($this->waitingWorkers[$tube])) {
                $workerId = $this->waitingWorkers[$tube];
                $params = $this->workers[$tube][$workerId];
                $this->selectNextJob(
                    $tube,
                    $workerId,
                    $params['connection'],
                    $params['keep-alive']
                );
            }
            $connection->end();
            return;
        } elseif ($datas[0] === 'exit') {
            throw new StopException('exit requested');
        } elseif ($datas[0] === 'stop') {
            if (isset($this->workers[$datas[1]][$datas[2]])) {
                $this->workers[$datas[1]][$datas[2]]['connection']->write("end\r\n");
            }
            $connection->end();
            return;
        } elseif ($datas[0] === 'kill') {
            $this->killWorker($datas[2]);
            return;
        } elseif ($datas[0] === 'stop-all-workers') {
            foreach ($this->workers as $workers) {
                foreach ($workers as $params) {
                    $params['connection']->write("end\r\n");
                }
            }
            $connection->end();
            return;
        } elseif ($datas[0] === 'test') {
            $this->io->out('test');
            return;
        } elseif ($datas[0] === 'error') {
            $this->io->error(substr($data, 6));
            return;
        } elseif ($datas[0] === 'ping') {
            $connection->write("pong\r\n");
            return;
        } elseif ($datas[0] === 'create-worker') {
            $this->initializeWorker($datas[1], $datas[2] ?? false);
            return;
        }
        if ($datas[0] === 'register') {
            $this->registerWorker($datas, $connection);
            return;
        } elseif ($datas[0] === 'release-job') {
            $job = $BeanstalkJobs->get($datas[1]);
            $prevState = $job->get('job_state');
            $BeanstalkJobs->transitionOrFail($job, BeanstalkJobsTable::T_CANCEL);
            $BeanstalkJobs->saveOrFail($job);
            $this->io->out(sprintf('job (id=%d) canceled', $datas[1]));
            $this->jobStateChanged($job, $prevState);
        } elseif ($datas[0] === 'bury-job') {
            $job = $BeanstalkJobs->get($datas[1]);
            $prevState = $job->get('job_state');
            $job->set('errors', $datas[2]);
            $BeanstalkJobs->transitionOrFail($job, BeanstalkJobsTable::T_FAIL);
            $BeanstalkJobs->saveOrFail($job);
            $this->io->out(sprintf('job (id=%d) failed', $datas[1]));
            $this->jobStateChanged($job, $prevState);
        } elseif ($datas[0] === 'delete-job') {
            $job = $BeanstalkJobs->get($datas[1]);
            $BeanstalkJobs->delete($job);
            $this->io->out(sprintf('job (id=%d) done', $datas[1]));
        } else {
            $this->io->warning("unknown message");
        }
        $connection->end();
    }

    /**
     * Lance un worker
     * @param string $tube
     * @return void
     */
    private function launchWorker(string $tube)
    {
        $config = Configure::read('Beanstalk.workers.'.$tube);
        if (!$config) {
            $this->io->out("$tube does not exist");
            return;
        }
        if (!($config['active'] ?? false)) {
            $this->io->out("$tube is inactive");
            return;
        }

        $command = preg_replace(
            '/^bin\/cake(\.bat|\.php)?/',
            ROOT.DS.'bin'.DS.'cake',
            $config['run']
        );
        $pid = exec(
            sprintf(
                '%s >>%s 2>&1 & echo $!',
                $command,
                LOGS . 'worker_' . $tube . '.log'
            )
        );
        $this->io->out("launched worker $tube (pid=$pid)");
    }

    /**
     * Retire un worker de la liste des workers
     * @param ConnectionInterface $connection
     * @return void
     * @throws ZMQSocketException
     */
    private function removeRef(ConnectionInterface $connection)
    {
        foreach ($this->workers as $tube => $workers) {
            foreach ($workers as $id => $params) {
                if ($params['connection'] === $connection) {
                    $this->io->out("terminated: $tube (id=$id)");
                    unset($this->workers[$tube][$id]);
                    $this->emit(
                        'worker_stop',
                        [
                            'tube' => $tube,
                            'id' => $id,
                        ]
                    );
                }
                if (empty($this->workers[$tube])) {
                    unset($this->workers[$tube]);
                }
            }
        }
    }

    /**
     * Sélectionne le prochain job disponnible
     * @param string              $tube
     * @param int                 $workerId
     * @param ConnectionInterface $connection
     * @param bool                $keepAlive
     * @return void
     * @throws Exception
     */
    private function selectNextJob(
        string $tube,
        int $workerId,
        ConnectionInterface $connection,
        bool $keepAlive
    ) {
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs', $this->modelParams);
        $jobs = $BeanstalkJobs->find()
            ->where(
                [
                    'tube' => $tube,
                    'job_state' => BeanstalkJobsTable::S_PENDING,
                ]
            )
            ->order(
                [
                    'priority',
                    'id',
                ]
            );
        /** @var EntityInterface $job */
        foreach ($jobs as $job) {
            $prevState = $job->get('job_state');
            $BeanstalkJobs->transitionOrFail($job, BeanstalkJobsTable::T_WORK);
            $job->set('beanstalk_worker_id', $workerId);
            $BeanstalkJobs->saveOrFail($job);
            $connection->write(sprintf("job:%d\r\n", $job->id));
            $this->io->out(
                sprintf(
                    'job (id=%d) in worker %s (id=%d)',
                    $job->id,
                    $job->get('tube'),
                    $workerId
                )
            );
            $this->jobStateChanged($job, $prevState);
            return;
        }
        // si il n'y a pas de jobs dispo, on ferme le worker
        $this->io->out('no more job in ' . $tube);
        $this->io->out('keep-alive ? ' . ($keepAlive ? 'yes' : 'no'));
        if (!$keepAlive) {
            $connection->write("end\r\n");
            $connection->end();
        } else {
            $this->workers[$tube][$workerId]['waiting'] = true;
        }
    }

    /**
     * Ensemble de traitements visan à remettre en ready les jobs qui en ont
     * besoin ainsi que lancer les workers qui manquent.
     * @return void
     * @throws Exception
     */
    public function checkReadyJobs()
    {
        $this->readConfig();
        if (static::isInterrupted()) {
            return;
        }
        /** @var BeanstalkJobsTable $BeanstalkJobs */
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs', $this->modelParams);
        /** @var BeanstalkWorkersTable $BeanstalkWorkers */
        $BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers', $this->modelParams);
        $ids = $BeanstalkWorkers->find('list', ['valueField' => 'id'])->disableHydration()->toArray();

        // On retire les connexions aux workers fermés
        foreach ($this->workers as $workers) {
            foreach ($workers as $id => $params) {
                if (!in_array($id, $ids)) {
                    $params['connection']->close();
                }
            }
        }

        // On libère les jobs qui n'ont plus de worker
        $jobs = $BeanstalkJobs->find()
            ->where(
                [
                    'job_state' => BeanstalkJobsTable::S_WORKING,
                    'beanstalk_worker_id IS' => null,
                ]
            );
        $cooldown = new DateInterval('PT10S');
        $now = new DateTime;
        /** @var EntityInterface $job */
        foreach ($jobs as $job) {
            /** @var FrozenTime $lastStateUpdate */
            $lastStateUpdate = $job->get('last_state_update');
            if (!$lastStateUpdate || $lastStateUpdate->add($cooldown) > $now) {
                continue;
            }
            $prevState = $job->get('job_state');
            $BeanstalkJobs->transitionOrFail($job, BeanstalkJobsTable::T_CANCEL);
            $BeanstalkJobs->saveOrFail($job);
            $this->jobStateChanged($job, $prevState);
        }

        // Les jobs qui ont passé leur délai deviennent disponnible
        $delayed = $BeanstalkJobs->find()
            ->where(['job_state' => BeanstalkJobsTable::S_DELAYED]);
        $now = new FrozenTime;
        /** @var EntityInterface $job */
        foreach ($delayed as $job) {
            /** @var FrozenTime $created */
            $created = $job->get('created');
            $seconds = new DateInterval('PT'.$job->get('delay').'S');
            if ($created->add($seconds) > $now) {
                continue; // pas encore dispo
            }
            $prevState = $job->get('job_state');
            $BeanstalkJobs->transitionOrFail($job, BeanstalkJobsTable::T_READY);
            $BeanstalkJobs->saveOrFail($job);
            $this->jobStateChanged($job, $prevState);
        }

        // On liste les tubes qui ont des jobs ready
        $tubes = $BeanstalkJobs->find()
            ->select(['tube'])
            ->where(['job_state' => BeanstalkJobsTable::S_PENDING])
            ->disableHydration()
            ->distinct(['tube'])
            ->all()
            ->map(fn(array $data) => $data['tube'])
            ->toArray();

        // On regarde si on a des workers en attente sur ces tubes
        foreach ($tubes as $tube) {
            foreach ($this->workers[$tube] ?? [] as $workerId => $params) {
                if ($params['waiting']) {
                    $this->workers[$tube][$workerId]['waiting'] = false;
                    $this->selectNextJob(
                        $tube,
                        $workerId,
                        $params['connection'],
                        $params['keep-alive']
                    );
                }
            }
        }

        // On supprime les workers qui ne tournent plus
        $BeanstalkWorkers->sync();

        if (!$tubes) {
            return;
        }
        $counts = array_fill_keys($tubes, 0);

        // Lance de nouveaux workers si on en a pas assez
        $q = $BeanstalkWorkers->query();
        $workers = $BeanstalkWorkers->find()
            ->select(
                [
                    'count' => $q->func()->count('*'),
                    'tube',
                ]
            )
            ->where(['tube IN' => $tubes])
            ->group(['tube']);
        foreach ($workers as $worker) {
            $counts[$worker->get('tube')] = $worker->get('count');
        }
        foreach ($counts as $tube => $count) {
            if (!isset($this->config['workers'][$tube])) {
                continue;
            }
            $limit = (int)$this->config['workers'][$tube]['quantity'];
            if ($count < $limit) {
                $this->launchWorker($tube);
            }
        }
    }

    /**
     * Donne le chemin du fichier app_local.json (si il existe, null sinon)
     * @return string|null
     */
    private function getConfigFilename(): ?string
    {
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (!$pathToLocalConfig || !is_readable($pathToLocalConfig)) {
            return null;
        }
        $jsonPath = include $pathToLocalConfig;
        return is_file($jsonPath) ? $jsonPath : null;
    }

    /**
     * filemtime() n'est plus mis à jour en php, on fait donc appel à bash
     * @param string $path
     * @return int
     */
    private function filemtime(string $path): int
    {
        return (int)trim(exec('stat -c %Y ' . escapeshellarg($path)));
    }

    /**
     * Lecture du app_local.json en cours de fonctionnement
     * @return void
     * @throws Exception
     */
    private function readConfig()
    {
        $jsonPath = $this->getConfigFilename();
        if (!$jsonPath || $this->filemtime($jsonPath) === $this->configMtime) {
            return;
        }
        $this->configMtime = $this->filemtime($jsonPath);
        $json = json_decode(file_get_contents($jsonPath), true);
        if ($json) {
            $this->io->out('config update in progress...');
            self::reloadConfig();
            $this->config = Configure::read('Beanstalk') + [
                'server_host' => '127.0.0.1',
                'server_port' => 11300,
            ];
            $this->io->out('done.');
        }
    }

    /**
     * Recharge la config de la même façon que dans le bootstrap.php
     * @return void
     * @throws Exception
     */
    private static function reloadConfig()
    {
        Configure::load('app_default', 'default', false);
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (is_readable($pathToLocalConfig)) {
            $json = include $pathToLocalConfig;
        } elseif (is_readable('/data/config/app_local.json')) {
            $json = '/data/config/app_local.json';
        } elseif (is_readable(CONFIG . 'app_local.json')) {
            $json = CONFIG . 'app_local.json';
        }
        if (!empty($json) && is_file($json)) {
            $local = json_decode(file_get_contents($json), true);
            $configs = [
                Configure::read(),
                $local,
            ];
            $workdir = getcwd();
            chdir(CONFIG);
            foreach ($local['Config']['files'] ?? [] as $path) {
                if (!is_file($path)) {
                    throw new Exception("Config file not found");
                }
                $configs[] = include $path;
            }
            $merged = call_user_func_array(["Cake\Utility\Hash", "merge"], $configs);
            Configure::write($merged);
            chdir($workdir);
        }
    }

    /**
     * Permet d'envoyer un message sur le serveur Ratchet
     *
     * @param string $chanel
     * @param mixed  $message
     * @throws ZMQSocketException
     */
    private function emit(string $chanel, $message)
    {
        if (!$this->ratchetSocket) {
            return;
        }
        $emit = [
            'category' => $chanel,
            'message' => $message,
        ];
        $this->ratchetSocket->send(json_encode($emit), ZMQ::MODE_DONTWAIT);
    }

    /**
     * Donne le socket ZMQ
     * @throws ZMQSocketException
     * @return void
     */
    private function setRatchet()
    {
        $config = Configure::read('Ratchet.zmq');
        if (!$config) {
            return;
        }
        $protocol = $config['protocol'];
        $domain = $config['domain'];
        $port = $config['port'];
        if (($ip = gethostbyname($domain)) !== $domain) {
            $domain = $ip;
        }
        $dsn = "$protocol://$domain:$port";
        $context = new ZMQContext();
        $this->ratchetSocket = $context->getSocket(ZMQ::SOCKET_PUSH);
        $this->ratchetSocket->setSockOpt(ZMQ::SOCKOPT_LINGER, 30);
        $this->ratchetSocket->setSockOpt(ZMQ::SOCKOPT_MAXMSGSIZE, 255);
        $this->ratchetSocket->connect($dsn);
    }

    /**
     * Envoi par websocket, les informations de changement d'état de job
     * @param EntityInterface $job
     * @param string|null     $prevState
     * @return void
     */
    private function jobStateChanged(EntityInterface $job, ?string $prevState)
    {
        $this->io->out(
            sprintf(
                'job (id=%d) state changed: %s -> %s',
                $job->id,
                $prevState,
                $job->get('job_state')
            )
        );
    }

    /**
     * Enregistre un worker et lui attribu un job
     * @param array               $datas
     * @param ConnectionInterface $connection
     * @return void
     * @throws ZMQSocketException
     */
    private function registerWorker(array $datas, ConnectionInterface $connection)
    {
        $tube = $datas[1];
        $workerId = $datas[2];
        $pos = strpos($workerId, ':');
        if ($pos !== false) {
            $workerId = substr($datas[2], 0, $pos);
            $keepAlive = true;
        } else {
            $keepAlive = false;
        }
        if (!isset($this->workers[$tube])) {
            $this->workers[$tube] = [];
        }
        if (!isset($this->workers[$tube][$workerId])) {
            /** @var BeanstalkWorkersTable $BeanstalkJobs */
            $BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers', $this->modelParams);
            $this->io->out("registered worker $tube (id=$workerId)");
            $this->emit(
                'worker_start',
                [
                    'tube' => $tube,
                    'id' => $workerId,
                ] + $BeanstalkWorkers->get($workerId)->toArray(),
            );
        }
        $this->workers[$tube][$workerId] = [
            'connection' => $connection,
            'keep-alive' => $keepAlive,
            'waiting' => false,
        ];
        $this->selectNextJob($tube, $workerId, $connection, $keepAlive);
    }

    /**
     * Effectu un kill sur un worker
     * @param int $workerId
     * @return void
     * @throws Exception
     */
    private function killWorker(int $workerId)
    {
        /** @var BeanstalkWorkersTable $BeanstalkWorkers */
        $BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers', $this->modelParams);
        $worker = $BeanstalkWorkers->find()->where(['id' => $workerId])->first();
        if (!$worker) {
            return;
        }
        $killCommand = Configure::read(
            'Beanstalk.workers.' . $worker->get('name') . '.kill'
        );
        if (!$killCommand) {
            throw new Exception('kill command not found');
        }
        $killCommand = str_replace(
            ['{{pid}}', '{{hostname}}'],
            [$worker->get('pid'), $worker->get('hostname')],
            $killCommand
        );
        exec($killCommand);
        $BeanstalkWorkers->delete($worker);
    }

    /**
     * Lance un nouveau worker
     * @param string $tube
     * @param bool   $keepAlive
     * @return void
     * @throws Exception
     */
    private function initializeWorker(string $tube, bool $keepAlive)
    {
        $runCommand = Configure::read(
            'Beanstalk.workers.' . $tube . '.run'
        );
        if (!$runCommand) {
            throw new Exception('kill command not found');
        }
        $command = sprintf(
            '%s %s>>%s 2>&1 & echo $!',
            $runCommand,
            !empty($keepAlive) ? '--keep-alive ' : '',
            escapeshellarg(LOGS . 'worker_' . $tube . '.log')
        );
        exec($command); // bin/cake worker <tube>( --keep-alive)?
    }

    /**
     * Reprend en parti la logique du
     * AsalaeCore\Middleware\MaintenanceMiddleware
     * @return bool
     * @throws Exception
     */
    private static function isInterrupted()
    {
        if (Configure::read('Interruption.enable_workers')) {
            return false;
        }
        $interrupted = false;
        $config = Configure::read('Interruption', []);
        $config += [
            'enabled' => false,
            'scheduled' => [
                'begin' => null,
                'end' => null,
            ],
            'periodic' => [
                'begin' => null,
                'end' => null,
            ],
        ];
        $now = new DateTime();
        if ($config['enabled']) {
            $interrupted = true;
        }
        if ($config['scheduled']['begin'] && new DateTime($config['scheduled']['begin']) < $now) {
            $interrupted = true;
        }
        if ($interrupted && $config['scheduled']['end'] && new DateTime($config['scheduled']['end']) < $now) {
            $interrupted = false;
        }
        if (!$interrupted && $config['periodic']['begin'] && $config['periodic']['end']) {
            $begin = new DateTime($config['periodic']['begin']);
            $end = new DateTime($config['periodic']['end']);
            // chevauchement des jours ex: 23h30 - 01h30 => si now est > 23h30 ou < 01h30
            if ($begin > $end && ($now > $begin || $now < $end)) {
                $interrupted = true;
            } elseif ($begin < $now && $end > $now) {
                $interrupted = true;
            }
        }
        return $interrupted;
    }
}
