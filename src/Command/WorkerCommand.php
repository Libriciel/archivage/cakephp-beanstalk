<?php
/**
 * Shell de lancement des workers
 *
 * Permet de lancer un worker en fonction de la configuration et en assurant
 * le suivi sur base de donnée.
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace Beanstalk\Command;

use Beanstalk\Exception\CantWorkException;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Exception\StopException;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\I18n\FrozenTime as Time;
use Exception;
use React\EventLoop\Loop;
use React\EventLoop\StreamSelectLoop;
use React\Socket\ConnectionInterface;
use React\Socket\TcpConnector;
use Throwable;

/**
 * Classe du shell
 *
 * Contient une méthode unique qui utilise le premier paramètre pour choisir
 * quel worker lancer.
 *
 * Le nom du worker doit correspondre au nom dans la configuration sous la clef
 * "Beanstalk.workers".
 *
 * Le worker doit implémenter une interface définie dans la constante
 * WORKER_INTERFACE
 *
 * Le suivi en base de donnée est assurée, le pid sera enregistré en base puis
 * retiré lorsque le worker aura fini de fonctionner.
 */
class WorkerCommand extends Command
{
    /**
     * @var EntityInterface worker
     */
    protected $workerEntity;
    /**
     * @var Arguments
     */
    protected Arguments $args;
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();

        $parser->addArgument(
            'worker',
            [
                'help' => __("Nom du worker"),
            ]
        );
        $parser->addOption(
            'dir',
            [
                'help' => __("Dossier de la classe du worker"),
            ]
        );
        $parser->addOption(
            'suffix',
            [
                'default' => 'Worker',
                'help' => __("Permet de spécifier le dossier worker"),
            ]
        );
        $parser->addOption(
            'tube',
            [
                'help' => __("Nom du tube (par défaut: <nom du worker>)"),
            ]
        );
        $parser->addOption(
            'one-job',
            [
                'boolean' => true,
                'help' => __("N'effectue qu'un seul job - utile pour un worker sous docker"),
            ]
        );
        $parser->addOption(
            'unique',
            [
                'boolean' => true,
                'help' => __("Lance le worker seulement s'il est seul sur son tube"),
            ]
        );
        $parser->addOption(
            'table-workers',
            [
                'default' => Configure::read('Beanstalk.table_workers', 'Beanstalk.BeanstalkWorkers'),
                'help' => __("Permet de spécifier la table utilisée pour stocker les informations sur le worker lancé"),
            ]
        );
        $parser->addOption(
            'table-jobs',
            [
                'default' => Configure::read('Beanstalk.table_jobs', 'Beanstalk.BeanstalkJobs'),
                'help' => __("Permet de spécifier la table utilisée pour stocker les informations sur le job"),
            ]
        );
        $parser->addOption(
            'log-file',
            [
                'help' => __("Fichier log du worker default: logs/worker_<tube>.log>"),
            ]
        );
        $parser->addOption(
            'keep-alive',
            [
                'help' => __("Ne termine pas automatiquement ce worker"),
                'boolean' => true,
            ]
        );
        $parser->setDescription(
            [
                "--------------------------------------------------------------------",
                __("Le cron 'worker' permet de lancer un worker type Beanstalk"),
                "--------------------------------------------------------------------",
            ]
        );
        return $parser;
    }

    /**
     * Méthode principale
     *
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @return void The exit code or null for success
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->args = $args;
        $this->io = $io;
        // On récupère la liste des workers selon les valeurs par défaut ou le param dir
        $workers = WorkerCommand::availablesWorkers();
        $availables = array_keys($workers);
        $worker = $args->getArgument('worker');

        // On récupère le worker défini en s'assurant qu'il soit parmis les dispos
        if ($worker) {
            if (!in_array($worker, $availables)) {
                $io->warning(__("Le worker {0} n'a pas été trouvé.", $worker));
                $worker = null;
            }
        }
        if (!$worker) {
            $worker = strtolower($io->askChoice(__("Veuillez choisir un worker à lancer:"), $availables));
        }
        $tube = $args->getOption('tube') ?: $worker;
        $className = $workers[$worker];
        $BeanstalkWorkers = $this->fetchTable($args->getOption('table-workers'));

        if ($args->getOption('unique') && $BeanstalkWorkers->exists(['tube' => $tube])) {
            $io->abort(__("Un worker utilise actuellement le même tube"), self::CODE_SUCCESS);
        }

        // On récupère le worker défini en base ou on en crée un nouveau
        $hostname = gethostname();
        $data = [
            'title' => $worker,
            'name' => $worker,
            'tube' => $tube,
            'path' => $className,
            'pid' => getmypid(),
            'last_launch' => new Time,
            'hostname' => $hostname,
            'logfile' => $args->getOption('log-file')
                ?: Configure::read('App.paths.logs', LOGS).'worker_'.$tube.'.log',
        ];
        $this->workerEntity = $BeanstalkWorkers->newEntity($data);
        $BeanstalkWorkers->saveOrFail($this->workerEntity);
        register_shutdown_function([$this, 'shutdown']);

        $this->requestNextJob();
    }

    /**
     * Supprime l'entrée en base
     * @return void
     */
    public function shutdown()
    {
        if (!$this->workerEntity || empty($this->args)) {
            return;
        }
        try {
            $BeanstalkWorkers = $this->fetchTable(
                $this->args->getOption('table-workers')
            );
            $BeanstalkWorkers->delete($this->workerEntity);
            $this->workerEntity = null;
        } catch (\Exception $e) {
        }
    }

    /**
     * Donne la liste des workers Disponibles sur un ensemble de répertoires
     * @return array
     */
    public static function availablesWorkers()
    {
        $workers = [];
        foreach (Configure::read('Beanstalk.workers') as $tube => $params) {
            $workers[$tube] = $params['classname'];
        }
        ksort($workers);
        return $workers;
    }

    /**
     * Gestion des messages TCP
     * @param string              $data
     * @param ConnectionInterface $connection
     * @param array               $params
     * @return void
     * @throws Exception
     */
    protected function handleIncomingMessage(string $data, ConnectionInterface $connection, array $params)
    {
        try {
            if ($data === 'ping') {
                $this->out('ping command');
                $connection->write("pong\r\n");
                return;
            } elseif ($data === 'end') {
                $this->out('end command');
                $connection->end();
                return;
            } elseif ($data === 'getprerequisites') {
                $this->out('getprerequisites command');
                $this->submitPrerequisites();
                return;
            } elseif (strpos($data, ':') === false) {
                $this->out('unknown command');
                return;
            }

            $datas = explode(':', $data);
            if ($datas[0] === 'command') {
                $this->out($datas[0] . ' command');
                $this->{$datas[1]}();
                return;
            } elseif (!$datas || $datas[0] !== 'job') {
                $this->out('unknown job');
                return;
            }

            $BeanstalkJobs = $this->fetchTable(
                $this->args->getOption('table-jobs')
            );
            $job = $BeanstalkJobs->get((int)$datas[1]);

            /** @var WorkerInterface $workerRunner */
            $workerRunner = new $params['path']($this->workerEntity, $this->io);
            $data = $job->get('object_data');
            $workerRunner->out(sprintf("New job reserved ; job_id=%d", $job->id));
            $workerRunner->beforeWork($data);
        } catch (Throwable $e) {
            $message = sprintf(
                '%s:%d : %s',
                $e->getFile(),
                $e->getLine(),
                $e->getMessage()
            );
            $this->resetConnection();
            $connection->write(sprintf("error:%s\r\n", $message));
            $this->error($message);
            $connection->end();
            return;
        }
        try {
            $workerRunner->work($data);
            $this->emit('delete-job:'.$job->id);
            $this->success("Job finished");
        } catch (CantWorkException $e) {
            $this->resetConnection();
            $this->out("Job skipped");
            $this->emit('delete-job:'.$job->id);
        } catch (StopException $e) {
            $this->resetConnection();
            $this->emit('release-job:'.$job->id);
            $connection->end();
            $this->out("Shutdown triggered");
        } catch (Throwable $e) {
            $this->resetConnection();
            $this->emit('bury-job:'.$job->id.':'.$e);
            $this->error("Error: {$e->getMessage()}");
        } finally {
            try {
                $workerRunner->afterWork($data);
            } catch (Throwable $e) {
                $this->resetConnection();
                $this->error("Error: {$e->getMessage()}");
            }
        }
        $connection->write(
            sprintf(
                "register:%s:%d%s\r\n",
                $params['tube'],
                $params['id'],
                ($this->args->getOption('keep-alive') ? ':keep-alive' : '')
            )
        );
    }

    /**
     * Envoi les prérequis liés au worker
     * @return void
     */
    protected function submitPrerequisites()
    {
    }

    /**
     * Assure un bon néttoyage de la connection à la base de données pour éviter
     * les connections bloqués
     */
    protected function resetConnection()
    {
        /** @var Connection $conn */
        $conn = ConnectionManager::get('default');
        if ($conn->inTransaction()) {
            $conn->rollback();
        }
    }

    /**
     * Message sur socket
     * @param string $message
     * @return void
     */
    private function emit(string $message)
    {
        $loop = new StreamSelectLoop;
        $client = new TcpConnector($loop);
        $config = Configure::read('Beanstalk') + [
            'client_host' => '127.0.0.1',
            'client_port' => 11300,
        ];
        // converti un dns en ip (nécéssaire pour TcpConnector)
        if (@inet_pton($config['client_host']) === false) {
            $config['client_host'] = gethostbyname($config['client_host']);
        }
        $uri = sprintf('tcp://%s:%d', $config['client_host'], $config['client_port']);
        $client
            ->connect($uri)
            ->then(
                function (ConnectionInterface $connection) use ($message) {
                    $connection->write($message . "\r\n");
                    $connection->end();
                }
            )
            ->otherwise(
                function ($e) {
                    if ($e instanceof Exception) {
                        $this->io->err($e->getMessage());
                        throw $e;
                    }
                }
            );
        $loop->run();
    }

    /**
     * Envoi une demande de job
     * @return void
     */
    private function requestNextJob()
    {
        $params = $this->workerEntity->toArray();
        $params['shell']['params'] = $this->args->getArguments();
        $params['shell']['args'] = $this->args;
        $loop = Loop::get();
        $client = new TcpConnector($loop);
        $config = Configure::read('Beanstalk') + [
            'client_host' => '127.0.0.1',
            'client_port' => 11300,
        ];
        // converti un dns en ip (nécéssaire pour TcpConnector)
        if (@inet_pton($config['client_host']) === false) {
            $config['client_host'] = gethostbyname($config['client_host']);
        }
        $uri = sprintf('tcp://%s:%d', $config['client_host'], $config['client_port']);
        $client
            ->connect($uri)
            ->then(
                function (ConnectionInterface $connection) use ($params) {
                    $connection->write(
                        sprintf(
                            "register:%s:%d%s\r\n",
                            $params['tube'],
                            $params['id'],
                            ($this->args->getOption('keep-alive') ? ':keep-alive' : '')
                        )
                    );
                    $connection->on(
                        'data',
                        function ($data) use ($connection, $params) {
                            $data = trim($data);
                            foreach (explode("\r\n", $data) as $message) {
                                $this->handleIncomingMessage($message, $connection, $params);
                            }
                        }
                    );
                }
            )
            ->otherwise(
                function ($e) {
                    if ($e instanceof Exception) {
                        $this->io->err($e->getMessage());
                        throw $e;
                    }
                }
            );
        $loop->run();
    }

    /**
     * Ajoute des informations sur un message
     * @param string $message
     * @return string
     */
    protected function messageFormatter(string $message)
    {
        return sprintf(
            '%s %s:%d (id=%d) - %s',
            date('Y-m-d H:i:s'),
            $this->workerEntity->get('hostname'),
            $this->workerEntity->get('pid'),
            $this->workerEntity->id,
            $message
        );
    }

    /**
     * Message classique sur sortie STDOUT
     * @param string $message
     * @return void
     */
    public function out(string $message)
    {
        $this->io->out($this->messageFormatter($message));
    }

    /**
     * Message du succès sur sortie STDOUT
     * @param string $message
     * @return void
     */
    public function success(string $message)
    {
        $this->io->success($this->messageFormatter($message));
    }

    /**
     * Erreur sur sortie STDERR
     * @param string $message
     * @return void
     */
    public function error(string $message)
    {
        $this->io->error($this->messageFormatter($message));
    }
}
