<?php
/**
 * Beanstalk\Command\JobEditCommand
 */

namespace Beanstalk\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;
use Pheanstalk\PheanstalkInterface;

/**
 * Permet de modifier un job
 * ex: bin/cake job edit 4531 '{"user_id":5,"foo":"bar"}'
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobEditCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job edit';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'job_id',
            [
                'help' => __("Identifiant beanstalk du job"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'data',
            [
                'help' => __("Data du job au format json"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'priority',
            [
                'help' => __("Priorité du job"),
                'default' => PheanstalkInterface::DEFAULT_PRIORITY,
            ]
        );
        $parser->addOption(
            'delay',
            [
                'help' => __("Délai du job"),
                'default' => PheanstalkInterface::DEFAULT_DELAY,
            ]
        );
        $parser->addOption(
            'ttr',
            [
                'help' => __("Durée de la réservation"),
                'default' => PheanstalkInterface::DEFAULT_TTR,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Jobs = $this->fetchTable('BeanstalkJobs');
        $data = json_decode($args->getArgument('data'));
        $job = $Jobs->find()
            ->where(['BeanstalkJobs.id' => $args->getArgument('job_id')])
            ->first();
        if (!$job) {
            $io->abort(
                __(
                    "Le job (job_id={0}) n'a pas été trouvé en base de données",
                    $args->getArgument('job_id')
                )
            );
        }
        $Jobs->patchEntity(
            $job,
            [
                'data' => $data,
                'priority' => $args->getOption('priority'),
                'delay' => $args->getOption('delay'),
                'ttr' => $args->getOption('ttr'),
            ]
        );
        $Jobs->saveOrFail($job);
    }
}
