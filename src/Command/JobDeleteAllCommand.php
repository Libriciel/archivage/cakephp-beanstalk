<?php
/**
 * Beanstalk\Command\JobDeleteAllCommand
 */

namespace Beanstalk\Command;

use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Permet de supprimer tous les jobs en erreur (buried) d'un tube
 * ex: bin/cake job delete_all test --state buried
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobDeleteAllCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job delete_all';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'tube',
            [
                'help' => __("Tube sur lequel supprimer tous les jobs"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'state',
            [
                'help' => __(
                    "Etat des jobs à supprimer, "
                    ."utiliser 'any' pour les sélectionner tous"
                ),
                'default' => BeanstalkJobsTable::S_FAILED,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $conditions = ['tube' => $args->getArgument('tube')];
        if (($state = $args->getOption('state')) && $state !== 'any') {
            $conditions['job_state'] = $state;
        }
        $Jobs = $this->fetchTable('BeanstalkJobs');
        $count = $Jobs->find()->where($conditions)->count();
        $io->out(__("Suppression de {0} job(s)", $count));
        foreach ($Jobs->find()->where($conditions) as $job) {
            $Jobs->deleteOrFail($job);
        }
    }
}
