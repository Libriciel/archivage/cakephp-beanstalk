<?php
/**
 * Beanstalk\Command\JobCheckCommand
 */

namespace Beanstalk\Command;

use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Permet de visualiser les jobs en erreur
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobCheckCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job check';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->setDescription(
            "====================================================\n"
            . __(
                "Donne la liste des jobs en erreur, renvoi un code 0 si "
                . "aucun jobs n'est en erreur, 1 dans le cas contraire"
            )
            . "\n===================================================="
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $q = $BeanstalkJobs->find();
        $query = $BeanstalkJobs->find()
            ->select(
                [
                    'count' => $q->func()->count('*'),
                    'tube',
                ]
            )
            ->where(['job_state' => BeanstalkJobsTable::S_FAILED])
            ->group(['tube'])
            ->orderAsc('tube')
            ->disableHydration();
        $success = true;
        foreach ($query as $result) {
            $success = false;
            $io->err(
                __(
                    "Il y a {0} job(s) en erreur dans le tube {1}",
                    $result['count'],
                    $result['tube']
                )
            );
        }
        if ($success) {
            $io->success(__("Aucun job n'est en erreur"));
            $this->abort(self::CODE_SUCCESS);
        } else {
            $this->abort();
        }
    }
}
