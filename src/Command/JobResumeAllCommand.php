<?php
/**
 * Beanstalk\Command\JobResumeAllCommand
 */

namespace Beanstalk\Command;

use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Permet de relancer tous les jobs d'un tube
 * ex: bin/cake job resume_all test
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobResumeAllCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job resume_all';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'tube',
            [
                'help' => __("Tube sur lequel relancer tous les jobs"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $conditions = ['tube' => $args->getArgument('tube')];
        if (($state = $args->getOption('state')) && $state !== 'any') {
            $conditions['job_state'] = $state;
        }
        /** @var BeanstalkJobsTable $Jobs */
        $Jobs = $this->fetchTable('BeanstalkJobs');
        $conditions['job_state IN'] = array_keys(
            $Jobs->transitions[BeanstalkJobsTable::T_RESUME]
        );
        $count = $Jobs->find()->where($conditions)->count();
        $io->out(__("Lancement de {0} job(s)", $count));
        foreach ($Jobs->find()->where($conditions) as $job) {
            $Jobs->transition($job, BeanstalkJobsTable::T_RESUME);
            $Jobs->saveOrFail($job);
        }
    }
}
