<?php
/**
 * Beanstalk\Command\AbstractWorker
 */

namespace Beanstalk\Command;

use Beanstalk\Worker\BeanstalkV3Interface;
use Beanstalk\Exception\CantWorkException;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\CommandInterface;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debugger;
use Cake\Console\Exception\StopException;
use Cake\Database\Connection;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Exception;
use Pheanstalk\Exception\ServerException;
use Throwable;

/**
 * Squelette d'un worker
 *
 * Pour créer un worker standard, il est utile d'étendre cette classe
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AbstractWorker implements WorkerInterface
{
    /**
     * Permet de maintenir le worker en vie après son travail
     *
     * @var boolean
     */
    public $keepAlive;

    /**
     * @var string Titre/nom public du worker
     */
    public $title;

    /**
     * Paramètres additionnels
     *
     * @var array
     */
    public $params;

    /**
     * @var string|null chemin vers le fichier de log
     */
    public $logfile;

    /**
     * Instance du worker
     *
     * @var Beanstalk|BeanstalkV3Interface
     */
    protected $Worker;

    /**
     * @var int id du dernier job ignoré
     */
    protected $ignoredJobId;

    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
    }

    /**
     * deprecated
     */
    public function start()
    {
        $data = [];
        $this->io->out(
            sprintf(
                "Starting %s ; id=%d pid=%d",
                get_called_class(),
                (isset($this->params['id']) ? " ".$this->params['id'] : ''),
                getmypid()
            )
        );
        $this->resetConnection();

        $stats = [
            'min' => INF,
            'max' => 0,
            'moy' => 0,
            'times' => [],
            'start' => 0,
            'end' => 0,
            'jobs' => [
                'total' => 0,
                'command' => 0,
                'done' => 0,
                'skipped' => 0,
                'errors' => 0,
            ]
        ];

        $Jobs = TableRegistry::getTableLocator()->get('Beanstalk.BeanstalkJobs');
        $existingJob = false;
        do {
            $job = $Jobs->find()
                ->where(['tube' => $this->Worker->getTube()])
                ->order(['priority', 'id'])
                ->first();
            if ($job) {
                try {
                    $this->Worker->selectJob($job->get('jobid'));
                    $this->Worker->reserve();
                    $existingJob = true;
                    break;
                } catch (Exception $ex) {
                    $Jobs->deleteAll(['id' => $job->id]);
                }
            }
        } while ($job);

        while ($existingJob || $this->Worker->getNext()) {
            $existingJob = false;
            $stats['start'] = microtime(true);
            $this->io->out(sprintf("New job reserved ; jobid=%d", $this->Worker->getJob()->getId()));
            try {
                $data = $this->Worker->getData();
                if ($data === 'shutdown') {
                    $this->Worker->done();
                    $this->shutdown();
                } elseif ($data === 'getprerequisites') {
                    $this->submitPrerequisites();
                    $this->Worker->done();
                    $this->io->out("Prerequisites submited");
                    $this->chronoStop($stats, 'command');
                    continue;
                } elseif (isset($data['command'])
                    && $data['command'] === 'getstats'
                    && isset($data['beanstalk_worker_id'])
                    && $this->params['id'] === (int)$data['beanstalk_worker_id']
                ) {
                    $this->Worker->done();
                    $this->chronoStop($stats, 'command');
                    $this->submitGetstats(
                        $sta = [
                            'durations' => [
                                'min' => $stats['min'] === INF ? -1 : $stats['min'],
                                'max' => $stats['max'],
                                'moy' => $stats['moy'],
                            ],
                            'jobs' => $stats['jobs'],
                        ]
                    );
                    $this->io->out(Debugger::exportVar($sta));
                    continue;
                } elseif (isset($data['command']) && isset($data['beanstalk_worker_id'])) {
                    if (isset($this->params['id'])
                        && $this->params['id'] === (int)$data['beanstalk_worker_id']
                        && method_exists($this, $data['command'])
                    ) {
                        $this->Worker->done();
                        $this->io->out("Worker command executed");
                        call_user_func([$this, $data['command']], $data);
                    } elseif ($this->ignoredJobId && $this->ignoredJobId === $this->Worker->getJob()->getId()) {
                        $this->io->out(sprintf("The command for worker %d was not executed", $data['beanstalk_worker_id']));
                        $this->Worker->done();
                    } else {
                        $sleepTime = $this->params['sleep_time'] ?? 1;
                        $this->io->out(
                            sprintf(
                                "Worker command sent for id=%d (sleeping for %d second(s))",
                                $data['beanstalk_worker_id'],
                                $sleepTime
                            )
                        );
                        $this->ignoredJobId = $this->Worker->getJob()->getId();
                        $this->Worker->cancel();
                        sleep($sleepTime); //NOSONAR
                    }
                    $this->chronoStop($stats, 'command');
                    continue;
                }
                $this->beforeWork($data);
                $this->work($data);
                $this->afterWork($data);
                $this->Worker->done();
                $this->io->out("Job work done in ".$this->chronoStop($stats, 'done'));
            } catch (CantWorkException $ex) {
                $this->afterWork($data);
                $this->Worker->done();
                $this->io->out("Job skipped");
                $this->chronoStop($stats, 'skipped');
            } catch (StopException $e) {
                $this->io->out("Shutdown triggered");
                $this->Worker->getPheanstalk()->getConnection()->disconnect();
                return $e->getCode();
            } catch (Throwable $ex) {
                $this->resetConnection();// rollback avant d'éffacer le job
                $this->fail($ex, $data);
                $this->Worker->fail((string)$ex);
                $this->chronoStop($stats, 'errors');
            } finally {
                $this->resetConnection();
            }

            if (!$this->keepAlive || !$this->keepWorking($this->params)) {
                break;
            }
        }
        $this->io->out("Work finished");
        $this->Worker->getPheanstalk()->getConnection()->disconnect();
    }

    /**
     * Arret du chrono de durée du job
     * @param array  $stats
     * @param string $type
     * @return string
     */
    private function chronoStop(array &$stats, string $type): string
    {
        $stats['end'] = microtime(true);
        $duration = $stats['end'] - $stats['start'];

        if ($type === 'done') {
            $stats['times'][] = $duration;
            if (count($stats['times']) > 100) {
                array_shift($stats['times']);
            }
            $stats['moy'] = array_sum($stats['times']) / count($stats['times']);
            $stats['min'] = min($duration, $stats['min']);
            $stats['max'] = max($duration, $stats['max']);
        }
        $stats['jobs']['total']++;
        $stats['jobs'][$type]++;

        if ($duration < 1) {
            return round($duration * 1000).'ms';
        } elseif ($duration < 60) {
            return round($duration, 3).'s';
        } elseif ($duration < 3600) {
            return floor($duration / 60).'m '.round($duration % 60).'s';
        } else {
            $duration /= 60;
            return floor($duration / 60).'h '.round($duration % 60).'m';
        }
    }

    /**
     * Lancé avant work()
     * @param mixed $data
     */
    public function beforeWork($data)
    {
    }

    /**
     * Lancé après work()
     * @param mixed $data
     * @throws Exception
     */
    public function afterWork($data)
    {
        /** @var Connection $conn */
        $conn = ConnectionManager::get('default');
        if ($conn->inTransaction()) {
            $conn->rollback();
            throw new Exception(__("Une transaction est restée ouverte à la fin du job"));
        }
        gc_collect_cycles();
    }

    /**
     * deprecated
     *
     * @param mixed $params
     * @return boolean
     */
    public function keepWorking($params)
    {
        if (!empty($params['sleep']) && is_numeric($params['sleep'])) {
            sleep($params['sleep']); //NOSONAR
        }
        return $params;
    }

    /**
     * deprecated
     *
     * @param mixed $params
     * @return mixed
     * @throws ServerException
     */
    public function touch($params = [])
    {
        $this->Worker->touch();
        return $params;
    }

    /**
     * deprecated
     *
     * @param string $string to log
     */
    public function log($string)
    {
        $str = date('Y-m-d H:i:s').' - '.$string."\n";
        if ($this->logfile) {
            file_put_contents($this->logfile, $str, FILE_APPEND);
        }
        echo $str;
    }

    /**
     * deprecated
     *
     * @param Throwable $ex   Exception envoyé depui le bloc
     *                        start()
     * @param mixed     $data Data obtenu par Beanstalkd
     */
    public function fail(Throwable $ex, $data)
    {
        $this->io->out(
            sprintf(
                "Job work fail on %s:%d with message:\n%s",
                $ex->getFile(),
                $ex->getLine(),
                $ex->getMessage()
            )
        );
        $this->io->out($ex->getTraceAsString());
        Log::error((string)$ex);
    }

    /**
     * deprecated
     */
    public function shutdown()
    {
        throw new StopException('shutdown', CommandInterface::CODE_SUCCESS);
    }

    /**
     * deprecated
     */
    public function submitPrerequisites()
    {
    }

    /**
     * deprecated
     */
    public function submitGetStats(array $stats)
    {
    }

    /**
     * Assure un bon néttoyage de la connection à la base de données pour éviter
     * les connections bloqués
     */
    protected function resetConnection()
    {
        /** @var Connection $conn */
        $conn = ConnectionManager::get('default');
        if ($conn->inTransaction()) {
            $conn->rollback();
        }
    }

    /**
     * Assure que le job ne soit plus réservé lors de la coupure du worker
     */
    public function __destruct()
    {
        try {
            if ($this->Worker->getJob()) {
                $this->Worker->cancel();
            }
        } catch (Exception $e) {
        }
    }
}
