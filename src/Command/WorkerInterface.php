<?php
/**
 * Interface d'un worker
 *
 * Tout worker devrait implémenter cet interface afin de respecter une cohérence
 * entre les différents workers
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace Beanstalk\Command;

use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Exception;

/**
 * Interface d'un worker
 *
 * <b>start()</b> se charge de lancer la procédure.
 * <b>work()</b> effectue le travail
 * <b>keepWorking()</b> permet de relancer <b>work()</b>
 */
interface WorkerInterface
{
    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null);

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws CantWorkException
     */
    public function work($data);

    /**
     * Message classique sur sortie STDOUT
     * @param string $message
     * @return void
     */
    public function out(string $message);

    /**
     * Erreur classique sur sortie STDERR
     * @param string $message
     * @return void
     */
    public function error(string $message);

    /**
     * Lancé avant work()
     * @param mixed $data
     */
    public function beforeWork($data);

    /**
     * Lancé après work()
     * @param mixed $data
     * @throws Exception
     */
    public function afterWork($data);
}
