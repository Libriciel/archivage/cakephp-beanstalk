<?php
/**
 * Beanstalk\Worker\HashWorker
 */

namespace Beanstalk\Worker;

use Beanstalk\Command\WorkerInterface;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Exception;
use Throwable;

/**
 * Permet de tester les actions liés aux workers
 * Attend 1 minute par job
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TestWorker implements TestableWorkerInterface, WorkerInterface
{
    /**
     * @var string nom du fichier temporaire de test du worker
     */
    const TEST_FILENAME = 'dummy-test-hash-worker';

    /**
     * @var string /path/to/tmp/filename
     */
    private $tmpFilename;

    /**
     * TestWorker constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        $this->tmpFilename = sys_get_temp_dir() . DS . 'emittest-destination' . DS . self::TEST_FILENAME;
        $dir = dirname($this->tmpFilename);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        echo "working...\n";
        sleep($data['duration'] ?? 10); //NOSONAR
        if (isset($data['error']) && ((is_bool($data['error']) && $data['error'])
                || (is_numeric($data['error']) && rand(0, 100) <= $data['error'])) // NOSONAR
        ) {
            throw new Exception('test error');
        }
        file_put_contents($this->tmpFilename, 'this is a test file');
    }

    /**
     * Emission d'un message de test
     * A utiliser avant getTestResults()
     * @param array $params optionnel, si le test à besoin de paramètres
     * @throws Exception
     */
    public function emitTest(array $params = [])
    {
        $data = ['duration' => 5];
        if (is_file($this->tmpFilename)) {
            unlink($this->tmpFilename);
        }
        Beanstalk::getInstance($this->params['tube'])->emit($data);
    }

    /**
     * Permet d'obtenir les résultats du test
     * @return array ['success' => (bool), 'errors' => (array), 'message' => (string)]
     * @throws Exception
     */
    public function getTestResults(): array
    {
        $success = false;
        if (is_file($this->tmpFilename)) {
            $success = file_get_contents($this->tmpFilename) === 'this is a test file';
            unlink($this->tmpFilename);
        }
        return [
            'success' => $success,
            'error' => $success ? '' : "Result file not found"
        ];
    }

    /**
     * Permet de maintenir le worker en vie
     *
     * @param mixed $params
     * @return void
     */
    public function keepWorking($params)
    {
    }

    /**
     * Lance la boucle du worker
     * Appellera de façon consécutive, la fonction <b>work()</b> puis
     * la fonction <b>keepWorking()</b>.
     */
    public function start()
    {
    }

    /**
     * Permet de mettre à jour le timer
     *
     * @param array $params
     */
    public function touch($params = [])
    {
    }

    /**
     * Permet d'envoyer un message formaté dans le but de garnir le fichier log
     *
     * @param string $string to log
     */
    public function log($string)
    {
    }

    /**
     * Callback utilisé en cas d'Exception dans le bloc d'execution du worker
     *
     * @param Throwable $ex Exception envoyé depui le bloc
     *                        start()
     * @param mixed     $data Data obtenu par Beanstalkd
     */
    public function fail(Throwable $ex, $data)
    {
    }

    /**
     * Met fin à l'execution du worker
     */
    public function shutdown()
    {
    }

    /**
     * Permet d'obtenir les prérequis du worker et de savoir s'ils sont résolus
     * Enverra dans le tube prerequisites:
     *  [
     *      'tube' => 'montube',
     *      'date' => '2018-04-25T14:05:11',
     *      'prerequisites' => ['mon prérequis' => true, ...]
     *  ]
     */
    public function submitPrerequisites()
    {
    }

    /**
     * Permet d'obtenir des stats d'un worker
     * @param array $stats
     * @return void
     */
    public function submitGetstats(array $stats)
    {
    }

    /**
     * Message classique sur sortie STDOUT
     * @param string $message
     * @return void
     */
    public function out(string $message)
    {
    }

    /**
     * Erreur classique sur sortie STDERR
     * @param string $message
     * @return void
     */
    public function error(string $message)
    {
    }

    /**
     * Lancé avant work()
     * @param mixed $data
     */
    public function beforeWork($data)
    {
    }

    /**
     * Lancé après work()
     * @param mixed $data
     * @throws Exception
     */
    public function afterWork($data)
    {
    }
}
