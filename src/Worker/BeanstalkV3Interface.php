<?php
/**
 * Beanstalk\Worker\BeanstalkV3Interface
 */

namespace Beanstalk\Worker;

use Beanstalk\Utility\Beanstalk;
use Cake\ORM\Query;
use Pheanstalk\Exception\ServerException;
use Pheanstalk\Job;
use Pheanstalk\PheanstalkInterface;
use Pheanstalk\Response;

/**
 * Liste des methodes d'un Beanstalk v3
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface BeanstalkV3Interface
{
    /**
     * Constructeur du client beanstalk
     * Effectue la connexion au serveur
     *
     * @param string $tube
     * @param array  $params
     */
    public function __construct($tube = PheanstalkInterface::DEFAULT_TUBE, array $params = []);

    /**
     * Permet de vérifier de façon statique que l'on est connecté
     * A n'utiliser que si on a pas besoin d'une instance de la classe
     * @param string $tube
     * @param array  $params
     * @return bool
     */
    public static function connected($tube = PheanstalkInterface::DEFAULT_TUBE, array $params = []);

    /**
     * Vrai si la connexion avec le service Beanstalkd est établie
     * @return bool
     */
    public function isConnected();

    /**
     * Setter du Pheanstalk
     * @param string $host
     * @param int    $port
     * @param null   $connectTimeout
     * @param bool   $connectPersistent
     * @return Beanstalk
     */
    public function setPheanstalk(
        $host = '127.0.0.1',
        $port = 11300,
        $connectTimeout = null,
        $connectPersistent = false
    );

    /**
     * Getter du Pheanstalk
     * @return PheanstalkInterface
     */
    public function getPheanstalk();

    /**
     * Donne l'état actuel du job
     * @return string|null
     */
    public function getJobStatus();

    /**
     * Donne le ttr du job
     * @return int|null
     */
    public function getTtr();

    /**
     * Getter du job en cours
     * @param bool $reserved
     * @return Job|null
     * @throws ServerException
     */
    public function getJob($reserved = false);

    /**
     * Détruit proprement Pheanstalk
     */
    public function __destruct();

    /**
     * Ajoute une clé salé au data pour vérifier la provenance du message
     * @param mixed $data
     * @return string
     */
    public function encodeData($data);

    /**
     * Envoi un message sur beanstalkd (avec clé de contrôle)
     * @param mixed $data
     * @param int   $priority
     * @param int   $delay
     * @param int   $ttr
     * @return int response id
     */
    public function emit(
        $data,
        $priority = PheanstalkInterface::DEFAULT_PRIORITY,
        $delay = PheanstalkInterface::DEFAULT_DELAY,
        $ttr = PheanstalkInterface::DEFAULT_TTR
    );

    /**
     * Supprime le job
     * @return self
     * @throws ServerException
     */
    public function done();

    /**
     * Enterre le job
     * state 'reserved' -> 'buried'
     * @var string $errors Motif de l'échec (sera stocké en base)
     * @return self
     * @throws ServerException
     */
    public function fail($errors = '');

    /**
     * Libère le job
     * state 'reserved' -> 'ready'
     * @return self
     * @throws ServerException
     */
    public function cancel();

    /**
     * Remet à zero le chrono du ttr (time to run)
     * Conserve ainsi la réservation
     * @return self
     * @throws ServerException
     */
    public function touch();

    /**
     * Libère un job
     * state 'buried' -> 'ready'
     * @return self
     * @throws ServerException
     */
    public function kick();

    /**
     * Récupère le job suivant
     * @param null $timeout
     * @return Beanstalk
     * @throws ServerException
     */
    public function getNext($timeout = null);

    /**
     * Récupère les données
     * @return mixed|bool
     * @throws ServerException
     */
    public function getData();

    /**
     * Sélectionne un job par son id, n'effectue pas de réservation
     * @param int $jobid
     * @return Beanstalk
     */
    public function selectJob($jobid);

    /**
     * Parcours les jobs
     * @return Beanstalk réussite de la réservation
     * @throws ServerException
     */
    public function reserve();

    /**
     * Permet d'obtenir des informations sur le job en cours
     * @return object|Response
     */
    public function statsJob();

    /**
     * Permet d'obtenir des informations sur le job en cours
     * @return object|Response
     */
    public function statsTube();

    /**
     * Getter de tube
     * @return string
     */
    public function getTube();

    /**
     * Vidange des jobs d'un tube
     * @param string|null $tube
     * @return Beanstalk
     */
    public function clearTube($tube = null);

    /**
     * Setter de tube
     * @param string $tube
     * @return Beanstalk
     */
    public function setTube($tube);

    /**
     * Donne le nombre de jobs en état "ready" (prêt à être réservés)
     * @return int
     */
    public function count();

    /**
     * Donne la liste des jobs enregistrés en base
     * @return Query
     */
    public function find();

    /**
     * Affiche une erreur si le TTR a été dépassé
     * @return string
     */
    public function checkTtr();
}
