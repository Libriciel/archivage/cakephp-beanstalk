<?php
/**
 * Beanstalk\Exception\CantWorkException
 */

namespace Beanstalk\Exception;

use Exception;

/**
 * Exception qui est lancée lorsqu'un job doit être considéré fait,
 * sans avoir pu aboutir.
 * Contrairement à une autre exception, le worker appellera la fonction
 * afterWork et supprimera le job.
 * Une autre exception "enterre" le job (état buried) mais ne le supprime pas.
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CantWorkException extends Exception
{
}
