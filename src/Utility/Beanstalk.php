<?php
/**
 * Beanstalk\Utility\Beanstalk
 */

namespace Beanstalk\Utility;

use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\Core\Configure;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Exception;
use Pheanstalk\PheanstalkInterface;
use React\EventLoop\StreamSelectLoop;
use React\Socket\ConnectionInterface;
use React\Socket\TcpConnector;

/**
 * Client Beanstalkd
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Beanstalk
{
    const TIMEOUT = 1.0;
    /**
     * @var array paramètres configurables
     */
    public $params;
    /**
     * @var string Tube utilisé
     */
    protected $tube;
    /**
     * @var BeanstalkJobsTable
     */
    public $BeanstalkJobs;
    /**
     * @var Beanstalk|null
     */
    public static ?Beanstalk $instance;

    /**
     * Constructeur du client beanstalk
     * Effectue la connexion au serveur
     *
     * @param string $tube
     * @param array  $params
     */
    public function __construct($tube = PheanstalkInterface::DEFAULT_TUBE, array $params = [])
    {
        $this->params = $params + [
            'tube' => $tube,
            'client_host' => Configure::read('Beanstalk.client_host', '127.0.0.1'),
            'client_port' => Configure::read('Beanstalk.client_port', PheanstalkInterface::DEFAULT_PORT),
            'timeout' => Configure::read('Beanstalk.timeout'),
            'persistant' => Configure::read('Beanstalk.persistant', false),
            'table_jobs' => Configure::read('Beanstalk.table_jobs', 'Beanstalk.BeanstalkJobs'),
            'table_workers' => Configure::read('Beanstalk.table_workers', 'Beanstalk.BeanstalkWorkers'),
            'disable_check_ttr' => Configure::read('Beanstalk.disable_check_ttr', true),
        ];
        // converti un dns en ip (nécéssaire pour TcpConnector)
        if (@inet_pton($this->params['client_host']) === false) {
            $this->params['client_host'] = gethostbyname($this->params['client_host']);
        }
        $this->tube = $tube;
        $loc = TableRegistry::getTableLocator();
        $this->BeanstalkJobs = $loc->get($this->params['table_jobs']);
    }

    /**
     * Getter statique
     * @param string|null $tube
     * @return Beanstalk
     */
    public static function getInstance(string $tube = null): Beanstalk
    {
        if (empty(self::$instance)) {
            self::$instance = new self;
        }
        if ($tube) {
            self::$instance->setTube($tube);
        }
        return self::$instance;
    }

    /**
     * Permet de vérifier de façon statique que l'on est connecté
     * A n'utiliser que si on a pas besoin d'une instance de la classe
     * @param string $tube
     * @return bool
     */
    public static function connected($tube = PheanstalkInterface::DEFAULT_TUBE)
    {
        return self::getInstance($tube)->isConnected();
    }

    /**
     * Vérifi la connexion
     * @return bool
     */
    public function isConnected(): bool
    {
        try {
            $response = $this->socketQuery('ping');
            return $response === 'pong';
        } catch (Exception $e) {
        }
        return false;
    }

    /**
     * Setter du tube
     * @param string $tube
     * @return $this
     */
    public function setTube(string $tube)
    {
        $this->tube = $tube;
        return $this;
    }

    /**
     * Envoi un message sur beanstalkd (avec clé de contrôle)
     * @param mixed $data
     * @param int   $priority
     * @param int   $delay
     * @param int   $ttr
     * @return int job id
     */
    public function emit(
        $data,
        $priority = PheanstalkInterface::DEFAULT_PRIORITY,
        $delay = PheanstalkInterface::DEFAULT_DELAY,
        $ttr = PheanstalkInterface::DEFAULT_TTR
    ) {
        // Extraction d'un model / foreign_key du data
        $loc = TableRegistry::getTableLocator();
        $model = $fk = null;
        foreach ((array)$data as $key => $value) {
            if ($key !== 'user_id'
                && is_numeric($value)
                && preg_match('/^(\w+)_id$/', $key, $m)
            ) {
                $modelName = Inflector::camelize(Inflector::pluralize($m[1]));
                if (get_class($loc->get($modelName)) !== Table::class) {
                    $model = $modelName;
                    $fk = (int)$value;
                    break;
                }
            }
        }

        $job = $this->BeanstalkJobs->newEntity(
            [
                'tube' => $this->tube,
                'priority' => $priority,
                'job_state' => $delay
                    ? BeanstalkJobsTable::S_DELAYED
                    : BeanstalkJobsTable::S_PENDING,
                'user_id' => $data['user_id'] ?? null,
                'delay' => $delay,
                'ttr' => $ttr,
                'data' => $data,
                'object_model' => $model,
                'object_foreign_key' => $fk,
            ]
        );
        $this->BeanstalkJobs->saveOrFail($job);
        $this->socketEmit(sprintf('job:%s:%d', $job->get('tube'), $job->id));
        return $job->id;
    }

    /**
     * Envoi un message au serveur
     * @param string $message
     * @return void
     */
    public function socketEmit(string $message)
    {
        $loop = new StreamSelectLoop;
        $tcpConnector = new TcpConnector($loop);
        $tcpConnector
            ->connect($this->getUri())
            ->then(
                function (ConnectionInterface $connection) use ($message) {
                    $connection->write($message . "\r\n");
                    $connection->end();
                }
            )
            ->otherwise(
                function ($e) {
                    if ($e instanceof Exception) {
                        throw $e;
                    }
                }
            );
        $loop->addTimer(self::TIMEOUT, fn() => $loop->stop());
        $loop->run();
    }

    /**
     * Envoi un message au serveur
     * @param string $message
     * @return void
     */
    public function socketQuery(string $message): string
    {
        $loop = new StreamSelectLoop;
        $tcpConnector = new TcpConnector($loop);
        $tcpConnector
            ->connect($this->getUri())
            ->then(
                function (ConnectionInterface $connection) use ($message, &$response) {
                    $connection->write($message . "\r\n");
                    $connection->on(
                        'data',
                        function ($data) use (&$response, $connection) {
                            $response = trim($data);
                            $connection->end();
                        }
                    );
                }
            );
        $loop->addTimer(self::TIMEOUT, fn() => $loop->stop());
        $loop->run();
        return $response ?? '';
    }

    /**
     * Donne le chemin vers le serveur
     * @return string
     */
    private function getUri(): string
    {
        return sprintf('tcp://%s:%d', $this->params['client_host'], $this->params['client_port']);
    }
}
