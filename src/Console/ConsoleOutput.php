<?php
/**
 * Beanstalk\Console\ConsoleOutput
 */

namespace Beanstalk\Console;

use Cake\Console\ConsoleOutput as CakeConsoleOutput;

/**
 * Permet d'exporter la sortie vers un fichier en plus du stdout et stderr
 *
 * @category    Console
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2023, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConsoleOutput extends CakeConsoleOutput
{
    /**
     * @var resource
     */
    private $externalLogs;
    /**
     * @var bool faux si le dernier message ne fini pas par un retour de ligne
     */
    private $nl = true;

    /**
     * Copie output vers le fichier $filename
     * @param string $filename
     * @return void
     */
    public function setExternalLog(string $filename)
    {
        $this->externalLogs = fopen($filename, 'a');
    }

    /**
     * Destructeur de classe
     */
    public function __destruct()
    {
        if ($this->externalLogs) {
            fclose($this->externalLogs);
        }
    }

    /**
     * Writes a message to the output stream.
     *
     * @param string $message Message to write.
     * @return int The number of bytes returned from writing to output.
     */
    protected function _write(string $message): int
    {
        if ($this->externalLogs) {
            fwrite(
                $this->externalLogs,
                ($this->nl ? date('Y-m-d H:i:s: ') : '')
                . $message
            );
        }
        $this->nl = $message && substr($message, -1, 1) === static::LF;
        return parent::_write($message);
    }
}
