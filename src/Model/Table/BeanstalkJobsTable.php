<?php
/**
 * Table beanstalk_jobs
 *
 * Défini la validation et assure la synchronisation avec le serveur beanstalk.
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace Beanstalk\Model\Table;

use ArrayObject;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use StateMachine\Model\Behavior\StateMachineBehavior;
use ZMQ;
use ZMQContext;
use ZMQSocket;
use ZMQSocketException;

/**
 * Classe de la table
 *
 * Si un job n'existe plus sur le serveur Beantalk, il est automatiquement
 * effacé de la base de donnée grâce à la méthode <b>sync()</b>.
 * @mixin StateMachineBehavior
 * @mixin TimestampBehavior
 */
class BeanstalkJobsTable extends Table
{
    const S_PENDING = 'pending';
    const S_WORKING = 'working';
    const S_FAILED = 'failed';
    const S_DELAYED = 'delayed';
    const S_PAUSED = 'paused';

    const T_WORK = 'work';
    const T_FAIL = 'fail';
    const T_RETRY = 'retry';
    const T_CANCEL = 'cancel';
    const T_READY = 'ready';
    const T_PAUSE = 'pause';
    const T_RESUME = 'resume';

    /**
     * @var array Changement d'êtats selon action
     */
    public $transitions = [
        self::T_WORK => [
            self::S_PENDING => self::S_WORKING,
        ],
        self::T_FAIL => [
            self::S_WORKING => self::S_FAILED,
        ],
        self::T_RETRY => [
            self::S_FAILED => self::S_PENDING,
        ],
        self::T_CANCEL => [
            self::S_WORKING => self::S_PENDING,
        ],
        self::T_READY => [
            self::S_DELAYED => self::S_PENDING,
        ],
        self::T_PAUSE => [
            self::S_DELAYED => self::S_PAUSED,
            self::S_PENDING => self::S_PAUSED,
        ],
        self::T_RESUME => [
            self::S_PAUSED => self::S_DELAYED,
            self::S_FAILED => self::S_PENDING,
        ],
    ];

    /**
     * @var string Etat initial
     */
    public $initialState = self::S_PENDING;
    /**
     * @var ZMQSocket|null
     */
    private ?ZMQSocket $ratchetSocket = null;

    /**
     * Constructeur
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->setRatchet();
    }

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine', ['fields' => ['state' => 'job_state']]);
        $this->setEntityClass('Beanstalk.BeanstalkJob');
        $this->belongsTo('BeanstalkWorkers');
        $this->belongsTo('Users');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('jobid')
            ->allowEmptyString('jobid', null, 'create');

        $validator
            ->add(
                'tube',
                'validFormat',
                [
                    'rule' => [
                        'custom',
                        "/^[a-z0-9\+\/;\.\$\(\)][a-z0-9\+\/;\.\$\(\)\-]*$/i"
                    ],
                    'message' => "Format de text invalid"
                ]
            )
            ->allowEmptyString('tube'); // default: "default"

        $validator
            ->allowEmptyString('data');

        $validator
            ->integer('priority')
            // @see vendor/pda/pheanstalk/doc/protocol-1.3.txt
            ->range('priority', [0, 4294967295])
            ->allowEmptyString('priority');

        $validator
            ->integer('delay')
            ->nonNegativeInteger('delay')
            ->allowEmptyString('delay');

        $validator
            ->integer('ttr')
            ->nonNegativeInteger('ttr')
            ->allowEmptyString('ttr');

        return $validator;
    }

    /**
     * La table est t-elle synchronisée ?
     *
     * @var boolean
     */
    public $sync = true;

    /**
     * Permet d'obtenir une instance de Beanstalk
     * @param string $tube
     * @return Beanstalk
     */
    public function getBeanstalk($tube = 'default')
    {
        $classname = Configure::read('Beanstalk.classname', Beanstalk::class);
        return new $classname($tube);
    }

    /**
     * Synchronise la base de donnée avec Beanstalk
     *
     * @return boolean
     */
    public function sync()
    {
        return true;
    }

    /**
     * Options Disponibles pour le champ selectionné
     *
     * @param string $field
     * @return array
     */
    public function options($field)
    {
        if ($field === 'state') {
            return [
                'ready' => __('Prêt'),
                'buried' => __('En erreur'),
                'delayed' => __('Programmé'),
                'reserved' => __('En cours')
            ];
        } elseif ($field === 'job_state') {
            return [
                self::S_PENDING => __("En attente"),
                self::S_WORKING => __("En cours"),
                self::S_FAILED => __("En erreur"),
                self::S_DELAYED => __("Programmé"),
                self::S_PAUSED => __("En pause"),
            ];
        }
        return [];
    }

    /**
     * Permet d'envoyer un message sur le serveur Ratchet
     *
     * @param string $chanel
     * @param mixed  $message
     * @throws ZMQSocketException
     */
    private function emit(string $chanel, $message)
    {
        if (!$this->ratchetSocket) {
            return;
        }
        $emit = [
            'category' => $chanel,
            'message' => $message,
        ];
        $this->ratchetSocket->send(json_encode($emit), ZMQ::MODE_DONTWAIT);
    }

    /**
     * Donne le socket ZMQ
     * @return void
     */
    private function setRatchet()
    {
        $config = Configure::read('Ratchet.zmq');
        if (!$config) {
            return;
        }
        $protocol = $config['protocol'];
        $domain = $config['domain'];
        $port = $config['port'];
        if (($ip = gethostbyname($domain)) !== $domain) {
            $domain = $ip;
        }
        $dsn = "$protocol://$domain:$port";
        try {
            $context = new ZMQContext();
            $this->ratchetSocket = $context->getSocket(ZMQ::SOCKET_PUSH);
            $this->ratchetSocket->setSockOpt(ZMQ::SOCKOPT_LINGER, 30);
            $this->ratchetSocket->setSockOpt(ZMQ::SOCKOPT_MAXMSGSIZE, 255);
            $this->ratchetSocket->connect($dsn);
        } catch (ZMQSocketException $e) {
            trigger_error((string)$e);
        }
    }

    /**
     * Envoi l'info de mise à jour via websocket
     * @param Event           $event
     * @param EntityInterface $entity
     * @param ArrayObject     $options
     * @return void
     * @throws ZMQSocketException
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterSaveCommit(
        Event $event,
        EntityInterface $entity,
        ArrayObject $options
    ) {
        if ($entity->isNew()) {
            $this->emit(
                'new_job',
                [
                    'tube' => $entity->get('tube'),
                    'id' => $entity->get('id'),
                    'state' => $entity->get('job_state'),
                ]
            );
        } elseif ($entity->isDirty('job_state')
            && $entity->get('job_state') !== $entity->getOriginal('job_state')
        ) {
            $this->emit(
                'job_state_changed',
                [
                    'tube' => $entity->get('tube'),
                    'state' => $entity->get('job_state'),
                    'prev_state' => $entity->getOriginal('job_state'),
                    'id' => $entity->get('id'),
                ]
            );
        }
    }

    /**
     * Envoi l'info de mise à jour via websocket
     * @param Event           $event
     * @param EntityInterface $entity
     * @param ArrayObject     $options
     * @return void
     * @throws ZMQSocketException
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterDeleteCommit(
        Event $event,
        EntityInterface $entity,
        ArrayObject $options
    ) {
        $this->emit(
            'delete_job',
            [
                'tube' => $entity->get('tube'),
                'id' => $entity->get('id'),
                'prev_state' => $entity->getOriginal('job_state'),
            ]
        );
    }
}
