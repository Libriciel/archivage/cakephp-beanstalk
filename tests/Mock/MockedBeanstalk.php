<?php

namespace Beanstalk\Test\Mock;

use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Pheanstalk\PheanstalkInterface;

class MockedBeanstalk extends Beanstalk
{
    public static $StaticPheanstalk;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct($tube = PheanstalkInterface::DEFAULT_TUBE, array $params = [])
    {
        $this->params = $params + [
            'tube' => $tube,
            'host' => Configure::read('Beanstalk.host', '127.0.0.1'),
            'port' => Configure::read('Beanstalk.port', PheanstalkInterface::DEFAULT_PORT),
            'timeout' => Configure::read('Beanstalk.timeout', null),
            'persistant' => Configure::read('Beanstalk.persistant', false),
            'table_jobs' => Configure::read('Beanstalk.table_jobs', 'Beanstalk.BeanstalkJobs'),
            'table_workers' => Configure::read('Beanstalk.table_workers', 'Beanstalk.BeanstalkWorkers'),
            'disable_check_ttr' => Configure::read('Beanstalk.disable_check_ttr', true),
        ];
        $this->tube = $tube;
        $this->BeanstalkJobs = TableRegistry::getTableLocator()->get($this->params['table_jobs']);
    }

    public function getPheanstalk()
    {
        return self::$StaticPheanstalk;
    }
}
