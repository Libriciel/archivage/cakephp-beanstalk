<?php
/**
 * Beanstalk\Test\Worker\TestWorker
 */

namespace Beanstalk\Test\Worker;

use Beanstalk\Command\WorkerInterface;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Exception;
use Throwable;

/**
 * Pour tests unitaires
 */
class TestWorker implements WorkerInterface
{
    public static $workData = [];
    public static $keepworkingData = [];
    public static $workDone = [];
    public $Beanstalk;

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     */
    public function work($data)
    {
        self::$workDone[] = $data;
    }

    /**
     * Permet de maintenir le worker en vie
     *
     * @param mixed $params
     * @return boolean
     */
    public function keepWorking($params)
    {
        return $params;
    }

    /**
     * Lance la boucle du worker
     * Appellera de façon consécutive, la fonction <b>work()</b> puis
     * la fonction <b>keepWorking()</b>.
     */
    public function start()
    {
        do {
            $this->work(current(self::$workData));
            $keepworking = current(self::$keepworkingData);
            next(self::$workData);
            next(self::$keepworkingData);
        } while ($this->keepWorking($keepworking));
    }

    /**
     * Permet de mettre à jour le timer
     *
     * @param array $params
     */
    public function touch($params = array())
    {
    }

    /**
     * Permet d'envoyer un message formaté dans le but de garnir le fichier log
     *
     * @param string $string to log
     */
    public function log($string)
    {
    }

    /**
     * Callback utilisé en cas d'Exception dans le bloc d'execution du worker
     *
     * @param Throwable $ex   Exception envoyé depui le bloc
     *                        start()
     * @param mixed     $data Data obtenu par Beanstalkd
     */
    public function fail(Throwable $ex, $data)
    {
    }

    /**
     * Met fin à l'execution du worker
     */
    public function shutdown()
    {
    }

    /**
     * Permet d'obtenir les prérequis du worker et de savoir s'ils sont résolus
     * Enverra dans le tube prerequisites:
     *  [
     *      'tube' => 'montube',
     *      'date' => '2018-04-25T14:05:11',
     *      'prerequisites' => ['mon prérequis' => true, ...]
     *  ]
     */
    public function submitPrerequisites()
    {
        return [];
    }

    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
    }

    /**
     * Permet d'obtenir des stats d'un worker
     * @param array $stats
     * @return mixed
     */
    public function submitGetstats(array $stats)
    {
    }

    /**
     * Message classique sur sortie STDOUT
     * @param string $message
     * @return void
     */
    public function out(string $message)
    {
    }

    /**
     * Erreur classique sur sortie STDERR
     * @param string $message
     * @return void
     */
    public function error(string $message)
    {
    }

    /**
     * Lancé avant work()
     * @param mixed $data
     */
    public function beforeWork($data)
    {
    }

    /**
     * Lancé après work()
     * @param mixed $data
     * @throws Exception
     */
    public function afterWork($data)
    {
    }
}
