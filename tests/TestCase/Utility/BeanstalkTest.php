<?php

namespace Beanstalk\Test\TestCase\Utility;

use Beanstalk\Test\Mock\MockedPheanstalk;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Class BeanstalkJobTest
 * @package Beanstalk\Test\Model
 */
class BeanstalkTest extends TestCase
{
    public $fixtures = [
        'app.BeanstalkJobs',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Beanstalk.PheanstalkClassname', MockedPheanstalk::class);
        Beanstalk::$instance = $this->getMockBuilder(Beanstalk::class)
            ->onlyMethods(['socketEmit'])
            ->getMock();
    }

    private function setIsConnected($bool)
    {
        /** @var MockObject|Beanstalk $instance */
        $instance = $this->getMockBuilder(Beanstalk::class)
            ->onlyMethods(['socketEmit', 'isConnected'])
            ->getMock();
        $instance->method('isConnected')->willReturn($bool);
        Beanstalk::$instance = $instance;
    }

    public function testConnected()
    {
        $this->setIsConnected(true);
        $this->assertTrue(Beanstalk::connected());

        $this->setIsConnected(false);
        $this->assertFalse(Beanstalk::connected());
    }

    public function testIsConnected()
    {
        $this->setIsConnected(false);
        $Beanstalk = Beanstalk::getInstance();
        $this->assertFalse($Beanstalk->isConnected());

        $this->setIsConnected(true);
        $Beanstalk = Beanstalk::getInstance();
        $this->assertTrue($Beanstalk->isConnected());
    }

    public function testEmit()
    {
        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $BeanstalkJobs->deleteAll([]);

        $Beanstalk = Beanstalk::getInstance('test');
        $jobid = $Beanstalk->emit('test');
        $this->assertEquals(1, $BeanstalkJobs->find()->count());

        $this->assertEquals($jobid, $BeanstalkJobs->find()->first()->get('id'));
    }
}
