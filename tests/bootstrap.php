<?php
/**
 * Test runner bootstrap.
 *
 * Add additional configuration/setup your application needs when running
 * unit tests in this file.
 */

use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Migrations\TestSuite\Migrator;

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/config/bootstrap.php';

ConnectionManager::alias('test', 'default');
try {
    $migrator = new Migrator();
    $migrator->run();
} catch (Exception $e) {
    debug($e);
    die;
}
TableRegistry::getTableLocator()->clear();
